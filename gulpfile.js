const gulp = require('gulp')
const livereload = require('gulp-livereload')

gulp.task('reload', () => {
  gulp.src('index.html')
    .pipe(livereload())
})

gulp.task('watch', () => {
  livereload.listen()
  gulp.watch(['public/**/*.css', 'public/**/*.js', 'index.html'], ['reload'])
})
