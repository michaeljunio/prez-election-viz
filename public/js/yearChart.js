/**
 * Constructor for the Year Chart
 *
 * @param electoralVoteChart instance of ElectoralVoteChart
 * @param tileChart instance of TileChart
 * @param votePercentageChart instance of Vote Percentage Chart
 * @param electionInfo instance of ElectionInfo
 * @param electionWinners data corresponding to the winning parties over mutiple election years
 */
function YearChart (electoralVoteChart, tileChart, votePercentageChart, electionWinners) {

  //Creating YearChart instance

  this.electoralVoteChart = electoralVoteChart
  this.tileChart = tileChart
  this.votePercentageChart = votePercentageChart
  // the data
  this.electionWinners = electionWinners
  this.init()
}

/**
 * Initializes the svg elements required for this chart
 */
YearChart.prototype.init = function () {
  const yearChartDiv = d3.select('#year-chart').classed('fullView', true)
  this.margin = { top: 10, right: 20, bottom: 30, left: 50 }

  //fetch the svg bounds
  this.svgBounds = yearChartDiv.node().getBoundingClientRect().width
  this.svgWidth = this.svgBounds - this.margin.left - this.margin.right
  this.svgHeight = 100

  //add the svg to the div
  this.svg = yearChartDiv.append('svg')
    .attr('width', this.svgBounds)
    .attr('height', this.svgHeight)
    .attr('viewBox', `0 0 ${this.svgWidth} ${this.svgHeight}`)

  d3.select(window).on('resize.one', () => {
    this.svg.attr('width', d3.select('#year-chart').node().getBoundingClientRect().width - this.margin.left - this.margin.right)
  })
}

/**
 * Returns the class that needs to be assigned to an element.
 *
 * @param party an ID for the party that is being referred to.
 */
YearChart.prototype.chooseClass = function (party) {
  switch (party) {
    case 'R':
      return 'yearChart republican'
    case 'D':
      return 'yearChart democrat'
    case 'I':
      return 'yearChart independent'
    default:
      return 'yearChart'
  }
}

/**
 * Creates a chart with circles representing each election year, populates text content and other required elements for the Year Chart
 */
YearChart.prototype.update = function () {
  //Domain definition for global color scale
  const domain = [-60, -50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50, 60]

  //Color range for global color scale
  const range = ['#063e78', '#08519c', '#3182bd', '#6baed6', '#9ecae1', '#c6dbef', '#fcbba1', '#fc9272', '#fb6a4a', '#de2d26', '#a50f15', '#860308']

  //Global colorScale be used consistently by all the charts
  this.colorScale = d3.scaleQuantile()
    .domain(domain)
    .range(range)

  this.electionWinners.forEach(d => {
    // Convert numeric values to 'numbers'
    d.year = +d.YEAR
    d.party = d.PARTY
    d.rdDiff = +d.RD_Difference
  })

  const radius = 20
  const yearScale = d3.scaleBand()
    .domain(this.electionWinners.map(d => d.year))
    .range([0, this.svgWidth])
    .padding(0.5)

  let lineScale = this.svg.selectAll('.lineChart').data([this.svgWidth])

  lineScale.exit().remove()

  lineScale = lineScale.enter().append('line')
      .classed('lineChart', true)
    .merge(lineScale)
      .attr('x1', 0)
      .attr('y1', radius + this.margin.top)
      .attr('x2', this.svgWidth)
      .attr('y2', radius + this.margin.top)

  let yearChart = this.svg.selectAll('.yearChart').data(this.electionWinners)

  yearChart.exit().remove()

  yearChart = yearChart.enter().append('circle')
      .classed('yearChart', true)
    .merge(yearChart)
      .attr('cx', d => yearScale(d.year))
      .attr('cy', radius + this.margin.top)
      .attr('r', radius)
      .attr('class', d => this.chooseClass(d.party))
      .on('mouseover', d1 => {
        d3.selectAll('.yearChart')
          .filter(d2 => d1.year == d2.year)
            .classed(`${this.chooseClass(d1.party)} highlighted`, true)
      })
      .on('mouseout', d1 => {
        d3.selectAll('.yearChart')
          .filter(d2 => d1.year == d2.year)
            .classed(this.chooseClass(d1.party), true)
        d3.selectAll('.yearChart')
          .filter(d2 => d1.year == d2.year)
            .classed('highlighted', false)
      })
      .on('click', d => this.updateCharts(d.year))

  let textChart = this.svg.selectAll('.textChart').data(this.electionWinners)

  textChart.exit().remove()

  textChart = textChart.enter().append('text')
      .classed('yeartext', true)
    .merge(textChart)
      .attr('x', d => yearScale(d.year))
      .attr('y', this.svgHeight - this.margin.bottom / 2)
      .text(d => d.year)

  this.updateCharts(2012)
}

YearChart.prototype.updateCharts = function (electionYear) {
  d3.csv(`data/Year_Timeline_${electionYear}.csv`, (error, electionResult) => {
    this.votePercentageChart.update(electionResult)
    this.electoralVoteChart.update(electionResult, this.colorScale)
    this.tileChart.update(electionResult, this.colorScale)

    d3.selectAll('.yearChart').classed('selected', false)
    d3.selectAll('.yearChart').filter(d => d.year == electionYear)
      .classed('selected', true)
  })
}
