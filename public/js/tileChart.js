/**
 * Constructor for the TileChart
 */
function TileChart () {
  this.init()
}

/**
 * Initializes the svg elements required to lay the tiles
 * and to populate the legend.
 */
TileChart.prototype.init = function () {
  const tilesDiv = d3.select('#tiles').classed('fullView', true)
  const svgBounds = tilesDiv.node().getBoundingClientRect()
  const legendHeight = 150
  const legend = d3.select('#legend').classed('fullView', true)

  this.margin = { top: 30, right: 20, bottom: 30, left: 50 }
  this.svgWidth = svgBounds.width - this.margin.left - this.margin.right
  this.svgHeight = this.svgWidth / 2
  this.legendSvg = legend.append('svg')
    .attr('width', this.svgWidth)
    .attr('height', legendHeight)
    .attr('viewBox', `0 0 ${this.svgWidth} ${legendHeight}`)
    .attr('transform', `translate(${this.margin.left}, 0)`)
    // .style('transform', 'scale(0.75)')
    .style('bgcolor', 'green')
  this.svg = tilesDiv.append('svg')
    .attr('width', this.svgWidth)
    .attr('height', this.svgHeight)
    .attr('viewBox', `0 0 ${this.svgWidth} ${this.svgHeight}`)
    .attr('transform', `translate(${this.margin.left}, 0)`)
    // .style('transform', 'scale(0.75)')
    .style('bgcolor', 'green')

  d3.select(window).on('resize.four', () => {
    this.legendSvg.attr('width', d3.select('#tileChart').node().getBoundingClientRect().width - this.margin.left - this.margin.right)
    this.svg.attr('width', d3.select('#tileChart').node().getBoundingClientRect().width - this.margin.left - this.margin.right)
  })
}

/**
 * Returns the class that needs to be assigned to an element.
 *
 * @param party an ID for the party that is being referred to.
 */
TileChart.prototype.chooseClass = function (party) {
  switch (party) {
    case 'R':
      return 'republican'
    case 'D':
      return 'democrat'
    case 'I':
      return 'independent'
    default:
      return ''
  }
}

/**
 * Renders the HTML content for tool tip.
 *
 * @param tooltipData information that needs to be populated in the tool tip
 * @return text HTML content for tool tip
 */
TileChart.prototype.renderTooltip = function (tooltipData) {
  return `
    <h2 class='${this.chooseClass(tooltipData.winner)}'>${tooltipData.state}</h2>
    Electoral Votes: ${tooltipData.electoralVotes}
    <ul>
      ${tooltipData.result.map(row => {
        return `
          <li class=${this.chooseClass(row.party)}>
            ${row.nominee}: \t\t ${row.votecount} (${row.percentage}%)
          </li>
        `
      }).join('')}
    </ul>
  `
}

/**
 * Creates tiles and tool tip for each state, legend for encoding the color scale information.
 *
 * @param electionResult election data for the year selected
 * @param colorScale global quantile scale based on the winning margin between republicans and democrats
 */
TileChart.prototype.update = function(electionResult, colorScale){
  const tileWidth = this.svgWidth / 12
  const tileHeight = this.svgHeight / 8
  const tip = d3.tip().attr('class', 'd3-tip')
    .direction('se')
    .offset(() => [0, 0])
    .html(d => {
      let result = []

      if (d.I_Votes !== '') {
        result.push({
          'nominee': d.I_Nominee_prop,
          'votecount': d.I_Votes,
          'percentage': d.I_Percentage,
          'party': 'I'
         })
      }
      result.push({
        'nominee': d.D_Nominee_prop,
        'votecount': d.D_Votes,
        'percentage': d.D_Percentage,
        'party': 'D'
      })
      result.push({
        'nominee': d.R_Nominee_prop,
        'votecount': d.R_Votes,
        'percentage': d.R_Percentage,
        'party': 'R'
       })
      return this.renderTooltip({
        'state': d.State,
        'electoralVotes': d.Total_EV,
        'winner': d.State_Winner,
        'result': result
      })
    })
  const legendQuantile = d3.legendColor()
    .shapeWidth((this.svgWidth - this.margin.right) / 12)
    .cells(12)
    .orient('horizontal')
    .scale(colorScale)

  this.legendSvg.append('g')
    .attr('class', 'legendQuantile')
    .attr('transform', 'translate(0, 50)')

  this.legendSvg.select('.legendQuantile')
    .call(legendQuantile)

  electionResult.map(d => {
    d.x = d.Space * tileWidth
    d.y = d.Row * tileHeight
    d.width = tileWidth
    d.height = tileHeight
    d.rdDiff = d.RD_Difference
  })

  let tiles = this.svg.selectAll('.tile').data(electionResult)

  tiles.exit().remove()

  tiles = tiles.enter().append('rect')
      .classed('tile', true)
    .merge(tiles)
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .attr('width', d => d.width)
      .attr('height', d => d.height)
      .style('fill', d => d.State_Winner == 'I' ? '#089c43' : colorScale(d.rdDiff))
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
      .call(tip)

  let tilesText = this.svg.selectAll('.tilestext').data(electionResult)

  tilesText.exit().remove()

  tilesText = tilesText.enter().append('text')
    .classed('tilestext',true).merge(tilesText)

  tilesText.selectAll('tspan').remove()

  tilesText
    .attr('y', d => d.y + d.height / 2)
    .append('tspan')
    .attr('x', d => d.x + d.width / 2)
    .attr('dx', 0)
    .attr('dy', -10)
    .text(d => d.Abbreviation)

  tilesText.append('tspan')
    .attr('x', d => d.x + d.width / 2)
    .attr('dy', 30)
    .text(d => d.Total_EV)
}
