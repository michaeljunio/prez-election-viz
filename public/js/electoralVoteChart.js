/**
 * Constructor for the ElectoralVoteChart
 *
 */
function ElectoralVoteChart () {
  this.init()
}

/**
 * Initializes the svg elements required for this chart
 */
ElectoralVoteChart.prototype.init = function () {
  const electoralVotesDiv = d3.select('#electoral-vote').classed('fullView', true)

  this.margin = { top: 30, right: 20, bottom: 30, left: 50 }
  this.svgBounds = electoralVotesDiv.node().getBoundingClientRect()
  this.svgWidth = this.svgBounds.width - this.margin.left - this.margin.right
  this.svgHeight = 150
  this.svg = electoralVotesDiv.append('svg')
    .attr('width', this.svgWidth)
    .attr('height', this.svgHeight)
    .attr('viewBox', `0 0 ${this.svgWidth} ${this.svgHeight}`)

  d3.select(window).on('resize.two', () => {
    this.svg.attr('width', d3.select('#electoral-vote').node().getBoundingClientRect().width - this.margin.left - this.margin.right)
  })
}

/**
 * Returns the class that needs to be assigned to an element.
 *
 * @param party an ID for the party that is being referred to.
 */
ElectoralVoteChart.prototype.chooseClass = function (party) {
  switch (party) {
    case 'R':
      return 'republican'
    case 'D':
      return 'democrat'
    case 'I':
      return 'independent'
    default:
      return ''
  }
}

/**
 * Renders the HTML content for tool tip
 *
 * @param tooltipData information that needs to be populated in the tool tip
 * @return text HTML content for toop tip
 */
ElectoralVoteChart.prototype.renderTooltip = function (tooltipData) {
  return `
    <h2 class='${this.chooseClass(tooltipData.winner)}'>${tooltipData.state}</h2>
    Electoral Votes: ${tooltipData.electoralVotes}
    <ul>
      ${tooltipData.result.map(row => {
        return `
          <li class=${this.chooseClass(row.party)}>
            ${row.nominee}: \t\t ${row.votecount} (${row.percentage}%)
          </li>
        `
      }).join('')}
    </ul>
  `
}

/**
 * Creates the stacked bar chart, text content and tool tips for electoral vote chart
 *
 * @param electionResult election data for the year selected
 * @param colorScale global quantile scale based on the winning margin between republicans and democrats
 */

ElectoralVoteChart.prototype.update = function (electionResult, colorScale) {
  const tip = d3.tip().attr('class', 'd3-tip')
    .direction('se')
    .offset(d => [0, 0])
    .html(d => {
      let result = []

      if (d.I_Votes !== '') {
        result.push({
          'nominee': d.I_Nominee_prop,
          'votecount': d.I_Votes,
          'percentage': d.I_Percentage,
          'party':'I'
        })
      }
      result.push({
        'nominee': d.D_Nominee_prop,
        'votecount': d.D_Votes,
        'percentage': d.D_Percentage,
        'party':'D'
      })
      result.push({
        'nominee': d.R_Nominee_prop,
        'votecount': d.R_Votes,
        'percentage': d.R_Percentage,
        'party': 'R'
      })

      return this.renderTooltip({
        'state': d.State,
        'electoralVotes': d.Total_EV,
        'winner': d.State_Winner,
        'result': result
      })
    })

  electionResult.map(d => d.rdDiff = d['RD_Difference'])

  let totalElectoralSeats = parseInt(electionResult[0].D_EV_Total) + parseInt(electionResult[0].R_EV_Total)

  if (electionResult[0].I_EV_Total != '') {
    totalElectoralSeats += parseInt(electionResult[0].I_EV_Total)
  }

  const electoralVoteScale = d3.scaleLinear()
    .domain([0, totalElectoralSeats])
    .range([0, this.svgWidth - electionResult.length])

  electionResult.sort((a, b) => {
    if (a.State_Winner != b.State_Winner) {
      if (a.State_Winner == 'I') {
        return -1
      } else if (b.State_Winner =='I') {
        return 1
      } else {
        return d3.ascending(a.State_Winner, b.State_Winner)
      }
    } else {
      return a.rdDiff - b.rdDiff
    }
  })

  let xCoordinateRect = 0

  electionResult.map((d, i) => {
    xCoordinateRect = xCoordinateRect + electoralVoteScale(parseInt(d.Total_EV))
    d.xValue = xCoordinateRect - electoralVoteScale(parseInt(d.Total_EV)) + i
  })

  let electoralVotes = this.svg.selectAll('.electoralVotes')
    .data(electionResult)

  electoralVotes.exit().remove()

  electoralVotes = electoralVotes.enter().append('rect')
      .classed('electoralVotes', true)
    .merge(electoralVotes)
      .call(tip)

  electoralVotes
    .attr('x', d => d.xValue)
    .attr('id', d => d.Abbreviation)
    .attr('width', d => electoralVoteScale(parseInt(d.Total_EV)))
    .attr('height', this.svgHeight / 4)
    .attr('y', this.margin.top * 2)
    .style('fill', d => d.State_Winner == 'I' ? '#089c43' : colorScale(d.rdDiff))
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide)

  const dividerText = ['Electoral Vote (270 needed to win)']
  let divider = this.svg.selectAll('.middlePoint').data(dividerText)

  divider.exit().remove()

  divider.enter().append('rect')
    .classed('middlePoint', true)
    .attr('x', this.svgWidth / 2)
    .attr('width', 4)
    .attr('height', this.svgHeight / 3)
    .attr('y', this.margin.top * 2 - ((this.svgHeight / 3 - this.svgHeight / 4)/ 2))

  let dividerNote = this.svg.selectAll('.electoralVotesNote').data(dividerText)

  dividerNote.exit().remove()

  dividerNote.enter().append('text')
    .classed('electoralVotesNote', 'true')
    .attr('x', (this.svgWidth - electionResult.length)/ 2)
    .attr('y', this.margin.top)
    .text(d => d)

  let electoralVoteShare = []

  if (electionResult[0].I_EV_Total != '') {
    electoralVoteShare.push({
      party: 'I',
      totalElectoralvotes: parseInt(electionResult[0].I_EV_Total)
    })
  }

  electoralVoteShare.push({
    party: 'D',
    totalElectoralvotes: parseInt(electionResult[0].D_EV_Total)
  })
  electoralVoteShare.push({
    party: 'R',
    totalElectoralvotes: parseInt(electionResult[0].R_EV_Total)
  })


  let electoralVoteText = this.svg.selectAll('.electoralVoteText').data(electoralVoteShare)

  electoralVoteText.exit().remove()

  electoralVoteText = electoralVoteText.enter().append('text')
    .classed('electoralVoteText', true).merge(electoralVoteText)

  let xCoordinateText = 0
  electoralVoteText
    .attr('x', (d, i) => {
      xCoordinateText = xCoordinateText + electoralVoteScale(d.totalElectoralvotes)
      return d.party == 'R' ? this.svgWidth : xCoordinateText - electoralVoteScale(d.totalElectoralvotes)
    })
    .attr('y', 1.5 * this.margin.top)
    .text(d => d.totalElectoralvotes)
    .attr('class', d =>`electoralVoteText ${this.chooseClass(d.party)}`)
}
