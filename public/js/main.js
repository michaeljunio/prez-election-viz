/*
 * Root file that handles instances of all the charts and loads the visualization
 */
(function(){
  const instance = null

  /**
   * Creates instances for every chart (classes created to handle each chart
   * the classes are defined in the respective javascript files.
   */
  function init () {
    const votePercentageChart = new VotePercentageChart()
    const tileChart = new TileChart()
    const electoralVoteChart = new ElectoralVoteChart()
    //load the data corresponding to all the election years
    //pass this data and instances of all the charts that update on year selection to yearChart's constructor
    d3.csv('data/yearwiseWinner.csv', (error, electionWinners) => {
      const yearChart = new YearChart(electoralVoteChart, tileChart, votePercentageChart, electionWinners)
      yearChart.update()
    })
  }

  /**
   *
   * @constructor
   */
  function Main () {
    if (instance != null) {
      throw new Error('Cannot instantiate more than one Class')
    }
  }

  /**
   *
   * @returns {Main singleton class |*}
   */
  Main.getInstance = function () {
    if (this.instance == null) {
      this.instance = new Main()

      //called when the class is initialized
      init()
    }
    return instance
  }

  Main.getInstance()
})()
