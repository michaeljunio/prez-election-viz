/**
 * Constructor for the Vote Percentage Chart
 */
function VotePercentageChart () {
  this.init()
}

/**
 * Initializes the svg elements required for this chart
 */
VotePercentageChart.prototype.init = function () {
  const votesPercentageDiv = d3.select('#votes-percentage').classed('fullView', true)

  this.margin = { top: 30, right: 20, bottom: 30, left: 50 }
  this.svgBounds = votesPercentageDiv.node().getBoundingClientRect()
  this.svgWidth = this.svgBounds.width - this.margin.left - this.margin.right
  this.svgHeight = 200
  this.svg = votesPercentageDiv.append('svg')
    .attr('width', this.svgWidth)
    .attr('height', this.svgHeight)
    .attr('viewBox', `0 0 ${this.svgWidth} ${this.svgHeight}`)

  d3.select(window).on('resize.three', () => {
    this.svg.attr('width', votesPercentageDiv.node().getBoundingClientRect().width - this.margin.left - this.margin.right)
  })
}

/**
 * Returns the class that needs to be assigned to an element.
 *
 * @param party an ID for the party that is being referred to.
 */
VotePercentageChart.prototype.chooseClass = function (party) {
  switch (party) {
    case 'R':
      return 'republican'
    case 'D':
      return 'democrat'
    case 'I':
      return 'independent'
    default:
      return ''
  }
}

/**
 * Renders the HTML content for tool tip
 *
 * @param tooltipData information that needs to be populated in the tool tip
 * @return text HTML content for toop tip
 */
VotePercentageChart.prototype.renderTooltip = function (tooltipData) {
  return `
    <ul>
    ${tooltipData.result.map(row => {
      return `
        <li class="${this.chooseClass(row.party)}">
          ${row.nominee}: \t\t ${row.votecount} (${row.percentage}%)
        </li>
      `
    }).join('')}
    </ul>
  `
}

/**
 * Creates the stacked bar chart, text content and tool tips for Vote Percentage chart
 *
 * @param electionResult election data for the year selected
 */
VotePercentageChart.prototype.update = function (electionResult) {
  const tip = d3.tip().attr('class', 'd3-tip')
    .direction('s')
    .offset(() => [0, 0])
    .html(d => {
      let result = []

      if (electionResult[0].I_Nominee_prop !== ' ') {
        result.push({
          'nominee': electionResult[0].I_Nominee_prop,
          'votecount': electionResult[0].I_Votes_Total,
          'percentage': electionResult[0].I_PopularPercentage,
          'party': 'I'
        })
      }
      result.push({
        'nominee': electionResult[0].D_Nominee_prop,
        'votecount': electionResult[0].D_Votes_Total,
        'percentage': electionResult[0].D_PopularPercentage,
        'party': 'D'
      })
      result.push({
        'nominee': electionResult[0].R_Nominee_prop,
        'votecount': electionResult[0].R_Votes_Total,
        'percentage': electionResult[0].R_PopularPercentage,
        'party': 'R'
      })

      return this.renderTooltip({ result })
    })

  const votesShare = []

  if (electionResult[0].I_PopularPercentage != '') {
    votesShare.push({
      party: 'I',
      percentage: parseFloat(electionResult[0].I_PopularPercentage),
      nominee: electionResult[0].I_Nominee_prop,
      votecount: electionResult[0].I_Votes
    })
  }

  votesShare.push({
    party: 'D',
    percentage: parseFloat(electionResult[0].D_PopularPercentage),
    nominee: electionResult[0].D_Nominee_prop,
    votecount: electionResult[0].D_Votes
  })
  votesShare.push({
    party: 'R',
    percentage: parseFloat(electionResult[0].R_PopularPercentage),
    nominee: electionResult[0].R_Nominee_prop,
    votecount: electionResult[0].R_Votes
  })

  const votesPercentageScale = d3.scaleLinear()
    .domain([0, 100])
    .range([0, this.svgWidth])

  let votesPercentage = this.svg.selectAll('.votesPercentage').data(votesShare)
  var xCoordinateRect = 0

  votesPercentage.exit().remove()

  votesPercentage = votesPercentage.enter().append('rect')
      .classed('votesPercentage', true)
    .merge(votesPercentage)
      .attr('x', d => {
        xCoordinateRect = xCoordinateRect + votesPercentageScale(d.percentage)
        return xCoordinateRect - votesPercentageScale(d.percentage)
      })
      .attr('width', d => votesPercentageScale(d.percentage))
      .attr('height', this.svgHeight / 4)
      .attr('y', this.margin.top * 2 + 50)
      .attr('class', d => `votesPercentage ${this.chooseClass(d.party)}`)
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
      .call(tip)

  const dividerText = ['Popular Vote (50%)']
  const divider = this.svg.selectAll('.middlePoint').data(dividerText)

  divider.exit().remove()

  divider.enter().append('rect')
    .classed('middlePoint', true)
    .attr('x', this.svgWidth / 2)
    .attr('width', 4)
    .attr('height', this.svgHeight / 3)
    .attr('y', this.margin.top * 2 - ((this.svgHeight/3- this.svgHeight / 4) / 2) + 50)

  const dividerNote = this.svg.selectAll('.votesPercentageNote').data(dividerText)

  dividerNote.exit().remove()

  dividerNote.enter().append('text')
    .classed('votesPercentageNote', 'true')
    .attr('x', this.svgWidth / 2)
    .attr('y', this.margin.top + 50)
    .text(d => d)


  let votesPercentageText = this.svg.selectAll('.votesPercentageText').data(votesShare)
  let xCoordinateText = 0

  votesPercentageText.exit().remove()

  votesPercentageText = votesPercentageText.enter().append('text')
      .classed('votesPercentageText', true)
    .merge(votesPercentageText)
      .attr('x', (d, i) => {
        xCoordinateText = xCoordinateText + votesPercentageScale(d.percentage)
        return d.party == 'R' ? xCoordinateText : xCoordinateText - votesPercentageScale(d.percentage) + i * 2 * this.margin.left
      })
      .attr('y', 1.5 * this.margin.top + 50)
      .text(d => `${d.percentage}%`)
      .attr('class', d => `votesPercentageText ${this.chooseClass(d.party)}`)

  const nomineeInfo = []

  if (electionResult[0].I_Votes_Total !== '') {
    nomineeInfo.push({
      party: 'I',
      nominee: electionResult[0].I_Nominee_prop
    })
  }

  nomineeInfo.push({
    party: 'D',
    nominee: electionResult[0].D_Nominee_prop
  })
  nomineeInfo.push({
    party: 'R',
    nominee: electionResult[0].R_Nominee_prop
  })

  let nomineeInfoText = this.svg.selectAll('.nomineeInfoText').data(nomineeInfo)

  nomineeInfoText.exit().remove()

  nomineeInfoText = nomineeInfoText.enter().append('text')
      .classed('nomineeInfoText', true)
    .merge(nomineeInfoText)

  nomineeInfoText
    .attr('x', (d, i, a) => {
      if (a.length > 2) {
        if (d.party == 'I') return 0
        if (d.party == 'D') return this.margin.left + i * .3 * this.svgWidth
        if (d.party == 'R') return this.svgWidth - this.margin.right
      } else {
        return d.party == 'D' ? 0 : this.svgWidth - this.margin.right
      }
    })
    .attr('y', this.margin.top)
    .text(d => d.nominee)
    .attr('class', d => `nomineeInfoText ${this.chooseClass(d.party)}`)

}
